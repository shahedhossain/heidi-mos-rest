package biz.shahed.freelance.heidi.mos.rest.groovy

class GroovyMessenger implements Messenger {

    GroovyMessenger() {}

    GroovyMessenger(String message) {
        this.message = message;
    }

    String message
    String anotherMessage
	
}
