package biz.shahed.freelance.heidi.mos.rest.groovy;

public interface Messenger {
	String getMessage();
}
