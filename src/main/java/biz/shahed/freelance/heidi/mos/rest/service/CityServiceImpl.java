package biz.shahed.freelance.heidi.mos.rest.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import biz.shahed.freelance.heidi.mos.model.rest.City;
import biz.shahed.freelance.heidi.mos.repo.rest.CityDao;
import biz.shahed.freelance.heidi.mos.util.component.BindBeanData;
import biz.shahed.freelance.heidi.mos.util.component.FormativeValidator;

@Service
@Transactional(readOnly = true)
public class CityServiceImpl implements CityService {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(CityServiceImpl.class);

	@Autowired
	private CityDao cityDao;
	
	@Autowired
	private FormativeValidator validator;
	
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
	
	@Autowired
	private BindBeanData<City> daoDataBind;
	

	@Override
	@Transactional(readOnly = false)
	public City destroy(City city) {
		City domain = city;
		cityDao.delete(domain);
		sessionFactory.getCurrentSession().flush();
		return domain;
	}

	
	@Override
	@Transactional(readOnly = false)
	public City destroy(Serializable serializable) {
		City city = cityDao.findById(serializable);
		return destroy(city);
	}


	@Override
	@Transactional(readOnly = true)
	public List<City> findAll() {
		List<City> list =  cityDao.findAll();
		return list;
	}


	@Override
	@Transactional(readOnly = true)
	public List<City> findAll(City city) {
		City where = city;
		return cityDao.findAll(where);
	}


	@Override
	@Transactional(readOnly = true)
	public City load(City city) {
		City where = city;
		return cityDao.find(where);
	}


	@Override
	@Transactional(readOnly = true)
	public City load(Serializable serializable) {
		return cityDao.get(serializable);
	}


	@Override
	@Transactional(readOnly = false)
	public City save(City city) {
		City domain = city;
		cityDao.save(domain);
		sessionFactory.getCurrentSession().flush();
		return domain;
	}


	@Override
	@Transactional(readOnly = false)
	public City update(City city) {
		City domain = city;
		cityDao.update(domain);
		sessionFactory.getCurrentSession().flush();
		return domain;
	}


	@Override
	@Transactional(readOnly = false)
	public City update(Serializable serializable, City city) {
		City domain = cityDao.findById(serializable);
		daoDataBind.copy(domain, city, new String[]{"id", "log", "version"});
		cityDao.update(domain);
		sessionFactory.getCurrentSession().flush();
		return domain;
	}
	
}
