package biz.shahed.freelance.heidi.mos.rest.task;


import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("attendance")
public class AttendanceCorn implements Serializable {
	
	private static final long serialVersionUID = -6516758908423844550L;
	
	private static final Logger log = LoggerFactory.getLogger(AttendanceCorn.class);
	
	public void printMe() {
		log.info("Corn: >> 0/5 * * * * ?");
	}
	
	public void printUs() {
		log.info("Corn: >> 3/10 * * * * ?");
	}
}

