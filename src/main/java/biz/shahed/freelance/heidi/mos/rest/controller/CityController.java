package biz.shahed.freelance.heidi.mos.rest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import biz.shahed.freelance.heidi.mos.pojo.rest.CityDto;
import biz.shahed.freelance.heidi.mos.sencha.SenchaData;
import biz.shahed.freelance.heidi.mos.sencha.SenchaFilter;
import biz.shahed.freelance.heidi.mos.sencha.SenchaPagination;
import biz.shahed.freelance.heidi.mos.sencha.SenchaSimpleData;
import biz.shahed.freelance.heidi.mos.sencha.service.SenchaDataService;
import biz.shahed.freelance.heidi.mos.util.DateUtil;
import biz.shahed.freelance.heidi.mos.util.JsonUtil;
import biz.shahed.freelance.heidi.mos.util.component.FormativeMessage;
import biz.shahed.freelance.heidi.mos.util.component.FormativeValidator;
import biz.shahed.freelance.heidi.mos.util.message.ViolatedMessage;

@Controller
@RequestMapping("/city")
public class CityController {

	private static final Logger log = LoggerFactory.getLogger(CityController.class);
	
	@Autowired
    private ServletContext context;		
	
	@Autowired
	private SenchaDataService<CityDto> modelDataService;
	
	@Autowired
	private SenchaDataService<List<CityDto>> listDataService;
	
	@Autowired
	private FormativeMessage message;
	
	@Autowired
	private FormativeValidator validator;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public @ResponseBody SenchaData<CityDto> create(@RequestBody SenchaSimpleData<CityDto> senchaData) throws Exception {
		
		CityDto pojo = senchaData.getData();
		
		log.info(pojo.getName());
		log.info("Date >>>" + pojo.getCreated());
		
		log.info(JsonUtil.objectAsString(pojo));
		
		return modelDataService.getSenchaData(CityDto.class, pojo);
	}
	
	@RequestMapping(value = "/store", method = RequestMethod.GET)
	public @ResponseBody SenchaData<List<CityDto>>  store(SenchaPagination pagination) throws Exception {
		int id = 0;
		List<CityDto> list = new ArrayList<CityDto>();
		CityDto pojo = new CityDto();
		pojo.setId(id++);
		pojo.setName("Electronics");
		list.add(pojo);
		
		pojo = new CityDto();
		pojo.setId(id++);
		pojo.setName("Electrical");
		list.add(pojo);
		
		pojo = new CityDto();
		pojo.setId(id++);
		pojo.setName("Computer");
		list.add(pojo);
		
		pojo = new CityDto();
		
		ViolatedMessage violation = validator.validate(pojo);
		if(violation.isViolated()){
			log.info(violation.getMessage());
		}
				
		log.info("page>>"+pagination.getPage() + "limit>>>"+pagination.getLimit());
		for(SenchaFilter filter:pagination.getSenchaFilters()){
			log.info("property>>>"+filter.getProperty() + "Class>>"+filter.getValue().getClass());
		}
		
		Date date = new Date();
		log.info("NOW >> " + date);
		log.info("MOR >> " + DateUtil.minDate(date));
		log.info("EVN >> " + DateUtil.maxDate(date));
		
		log.info("Message <<<<>>>" + message.getMessage("D00T002001.name"));
		return listDataService.getSenchaData(CityDto.class, list, list.size());
	}
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello(Model model) {
		log.info("Hello world");
		model.addAttribute("message", "Spring 3 MVC Hello World");
		return "hello";

	}
	
	@RequestMapping(value = "/world", method = RequestMethod.GET)
	public String world(Model model) {
		log.info("Hello world");
		model.addAttribute("message", "Spring 3 MVC Hello World");
		return "world";

	}
	
	@RequestMapping(value = "/iframe", method = RequestMethod.GET)
	public String iframe(Model model) {
		log.info("Hello world");
		model.addAttribute("message", "Spring 3 jQuery iframe layout");
		return "iframe";
	}
	
	@RequestMapping(value = "/layout", method = RequestMethod.GET)
	public String layout(Model model) {
		log.info("Hello world");
		model.addAttribute("message", "Spring 3 jQuery boader layout");
		return "layout";
	}

	@RequestMapping(value = "/complex", method = RequestMethod.GET)
	public String complex(Model model) {
		log.info("Hello world");
		model.addAttribute("message", "Spring 3 jQuery complex layout");
		return "complex";
	}
	
	@RequestMapping(value="/doit", method = RequestMethod.GET)
	public String doIt() {
	    return "doit";
	}
	
}