package biz.shahed.freelance.heidi.mos.rest.groovy;

public interface Calculator {
	int add(int x, int y);
}
