package biz.shahed.freelance.heidi.mos.rest.service;

import java.io.Serializable;
import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.City;

public interface CityService {
	
	City destroy(City city);
	
	City destroy(Serializable serializable);
	
	List<City> findAll();
	
	List<City> findAll(City city);
	
	City load(City city);
	
	City load(Serializable serializable);
	
	City save(City city);
	
	City update(City city);
	
	City update(Serializable serializable, City city);
}
