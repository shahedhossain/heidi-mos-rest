package biz.shahed.freelance.heidi.mos.rest.service;

import java.util.List;

import biz.shahed.freelance.heidi.mos.model.rest.City;

public interface CompanyService {
	
	List<City> findAll();

}
